import os, markdown, glob, sys, traceback, xxhash
import sqlite3
from contextlib import closing


DIR = os.path.dirname(__file__)
ARTICLES_FOLDER = os.path.abspath(
    os.path.join(DIR, os.pardir, os.pardir, 'articles.based.how'))
DB_FILE = os.path.abspath(
    os.path.join(DIR, os.pardir, 'articles.db'))
MD_EXTENSIONS=['sane_lists', 'fenced_code', 'tables', 'abbr', 'attr_list', 'def_list']
DB = None


def db():
    return closing(DB.cursor())


def init():
    print('Initializing database.')
    global DB
    _db = sqlite3.connect(DB_FILE, check_same_thread=False, uri=True)
    _db.row_factory = sqlite3.Row
    DB = _db
    
    print('Loading articles.')
    valuesArticles = []
    valuesTags = []
    valuesTagDesc = []
    
    # TODO: add deletion of articles if a file is not present.
    
    with db() as c:
        c.executescript('''
                create table if not exists articles (
                    title text primary key,
                    html blob,
                    hash text
                );
                
                create table if not exists tags (
                    article_title text, 
                    tag text,
                    primary key (article_title, tag)
                );
                
                create table if not exists tag_descriptions (
                    tag text primary key,
                    description text
                );
            ''')
    DB.commit()
    
    if not os.path.exists(ARTICLES_FOLDER):
        print('Error: %s not found.'%ARTICLES_FOLDER) #TODO replace with logging library
    
    article_files = [f for f in os.listdir(ARTICLES_FOLDER) if f.endswith('.md')]
    if len(article_files) == 0:
        print('Error: %s contains no articles.'%ARTICLES_FOLDER)
    
    for article_file in article_files:
        title = article_file.replace('.md','')
        article_path = os.path.join(ARTICLES_FOLDER, article_file)
        
        # avoid reading the same article into db.
        
        with open(article_path, 'r') as file:
            try:
                contents = file.read()
            except UnicodeDecodeError:
                print('Cannot read file [%s]:'%file.name)
                traceback.print_exc(limit=0)
                sys.exit(1)
            hash = xxhash.xxh64_hexdigest(contents)
        
        with db() as c:
            isSame = c.execute(
                    'select 1 from articles where title=? and hash=?',
                    [title, hash]
                ).fetchone()
        
        if isSame:
            continue
        
        with open(article_path, 'r') as file:
            try:
                lines = file.readlines()
            except:
                print('Failure to read [%s] into database:'%article_file)
                traceback.print_exc()
                continue # can't read the file.
            
            tags = [t.strip() for t in lines.pop(0).replace('tags:','').split(',')]
            
            for tag in tags:
                valuesTags.append((tag, title))
            
            md = ''.join(lines)
            html = markdown.markdown( md, extensions=MD_EXTENSIONS )
            
            valuesArticles.append((html, title))
    
    with db() as c:
        c.executemany('''
            insert or ignore into articles 
            (html, title) 
            values (?,?)
            ''', valuesArticles)
        c.executemany('''
            update articles 
            set html=? 
            where title=?
            ''', valuesArticles)
        
        c.executemany('''
            insert or ignore into tags 
            (tag, article_title) 
            values (?,?)
            ''', valuesTags)
        c.executemany('''
            update or ignore tags 
            set tag=? 
            where article_title=?
            ''', valuesTags)
    
    DB.commit()
    print('Articles loaded.')
    
    # read tag descriptions
    with open(os.path.join(ARTICLES_FOLDER, 'tag_descriptions.txt')) as file:
        try:
            for line in file.readlines():
                tag, desc = line.split(': ')
                valuesTagDesc.append((desc, tag))
        except:
            print('Tag descriptions [%s] is missing or cannot be read.'%file.name)
            traceback.print_exc()
    
    with db() as c:
        c.executemany('''
            insert or ignore into tag_descriptions 
            (description, tag) 
            values (?,?)
            ''', valuesTagDesc)
        c.executemany('''
            update or ignore tag_descriptions 
            set description=? 
            where tag=?
            ''', valuesTagDesc)
    
    DB.commit()
    print('Tag descriptions loaded.')


def get_all_titles():
    sql = 'select title from articles'
    with db() as c:
        return [x['title'] for x in c.execute(sql)]


def get_all_distinct_tags():
    sql = 'select distinct tag from tags'
    with db() as c:
        return [x[0] for x in c.execute(sql)]


def get_article_html(title):
    sql = 'select html from articles where title=?'
    with db() as c:
        row = c.execute(sql,(title,)).fetchone()
        return None if row is None else row['html']
        


def get_article_tags(title):
    sql = 'select tag from tags where article_title=?'
    with db() as c:
        return [x[0] for x in c.execute(sql, (title,))]
    

def get_articles_with_tag(tag):
    sql = 'select article_title from tags where tag=?'
    with db() as c:
        return [x['article_title'] for x in c.execute(sql, (tag,))]


def get_tag_description(tag):
    sql = 'select description from tag_descriptions where tag=?'
    with db() as c:
        row = c.execute(sql,(tag,)).fetchone()
        return None if row is None else row['description']


init()