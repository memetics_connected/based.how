#!/usr/bin/env python

import random

quotes = [
'''src="./static/img/aristoteles.png"
alt="Aristoteles looking at you."
title="Aristoteles on women:&#013;
Women are imperfect or maimed men.&#013;
Nonetheless, they must be treated with kindness and sympathy."''',

'''src="./static/img/aristoteles.png"
alt="Aristoteles looking at you."
title="Aristoteles on multiculturalism:&#013;
Despots have always reigned over highly fragmented societies.&#013;
A multi-ethnic society is thus necessarily anti-democratic and chaotic."''',

'''src="./static/img/diogenes.png"
alt="Diogenes next to his barrel."
title="Diogenes talks to Alexander the Great:&#013;
Alexander: I am the most powerful man in the world and will give you one thing!&#013;
Diogenes: Move, you are blocking the sun."''',

'''src="./static/img/diogenes.png"
alt="Diogenes next to his barrel."
title="Diogenes on the rich:&#013;
In a rich man's house there is no place to spit but his face."''',


'''src="./static/img/diogenes.png"
alt="Diogenes next to his barrel."
title="Diogenes on Plato and Man:&#013;
What a pretentious fucking cunt. He thinks man is a featherless chicken.&#013;
According to him, since both man and chicken walk, man is a chicken without feathers.&#013;
So anyways, I defeathered a chicken and showed it to his class.&#013;
That shut him up. What a retard."'''
]

def get_random():
    return random.choice(quotes)