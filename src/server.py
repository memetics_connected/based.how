#!/usr/bin/env python

# Flask framework
from flask import Flask, render_template, request, abort
from pyngrok import ngrok
from flask_mobility import Mobility

# other components
import quotes, articles_db


app = Flask(__name__)
Mobility(app)


@app.route('/')
def index():
    
    titles = articles_db.get_all_titles()
    tags = articles_db.get_all_distinct_tags()
    
    return render_template(
        'index.html', 
        articles=titles, 
        tags=tags, 
        quote= (quotes.get_random() if not request.MOBILE else '') )


@app.route('/articles/<title>')
def articles(title=''):
    
    article_body = articles_db.get_article_html(title)
    
    if article_body is None:
        abort(404)
    
    tags = articles_db.get_article_tags(title)
    
    return render_template(
        'article.html', 
        title=title.replace('_', ' '), 
        tags=tags, 
        body=article_body)


@app.route('/@<tag>')
def articles_by_tag(tag=''):
    
    titles = articles_db.get_articles_with_tag(tag)
    desc = articles_db.get_tag_description(tag)
    print(desc)
    return render_template(
        'articles_by_tag.html', 
        articles=titles,
        tag=tag,
        description=desc)


if __name__ == '__main__':
    app.run(debug=True, port=8080, host='0.0.0.0')